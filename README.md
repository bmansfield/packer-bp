# Packer Boilerplate

This is just my quick reference so I don't have to go making a new `packer`
project from scratch every time.


## About

This is by no means intended to work right out of the gate. That is not the goal.
The goal is something I can quickly copy and start taking the parts I needs/want,
and a place to start modifying for my new project, so that I don't have to go to
the documentation and hunt them down each time.

I tried to put in most of the basic common parts I use over and over again:

* different sources
* different variables such as "local"
* different builders such as "null", "amazon", and "docker"
* how different builders use such sources, and their common configs
* different provisionioners such as "ansible", "local inline shells", "scripts", and "files"
* how to use different common post-processors such as "compression", "inline-scripts", and "docker-tags"

This is by no means an extensive list. Just the most common parts. And as I use
this boilerplate more and more, I'll likely modify and add to it.


## Future Improvements

* I would like to flesh this out a little more. Probably things like a standard
  ubuntu one, a rhel STIG compliant one, etc.
* I would also like to build out the automation a little more, `Jenkinsfile`
  improve the `Makefile` a little more
* I would like to build out the `ansible` and `docker` parts to be a little more
  feature full.
* More samples of variables, how they get passed into the builds or useage of
  environment variables, or sourcing environment files. Especially the AWS ones
* IAM json

For now this is a good start. It already saves me a ton of work.

