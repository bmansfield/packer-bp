VAR_FILE := variables.json
CONFIG_FILE := ami.pkr.hcl

all: validate inspect build

default: validate build


.PHONY: validate
validate:
	packer validate \
		-var-file=$(VAR_FILE) \
		$(CONFIG_FILE)

.PHONY: inspect
inspect:
	packer inspect \
		-var-file=$(VAR_FILE) \
		$(CONFIG_FILE)

.PHONY: build install
build install:
	packer build \
		-var-file=$(VAR_FILE) \
		$(CONFIG_FILE)
	# with passed variables
	# packer build \
	# 	-var='env=TEST' \
	# 	-var-file=$(VAR_FILE) \
	# 	$(CONFIG_FILE)
	# capture output to file
	# packer build httpd.json 2>&1 | sudo tee output.txt
	# extract ami id to file
	# tail -2 output.txt | head -2 | awk 'match($0, /ami-.*/) { print substr($0, RSTART, RLENGTH) }' > sudo ami.txt
	# PACKER_LOG=1 packer build ...

.PHONY: ls list
ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

