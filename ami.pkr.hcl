packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "ami_id" {
  type    = string
  default = "ami-01e78c5619c5e68b4"
}

variable "ami_prefix" {
  type    = string
  default = "learn-packer-linux-aws-redis"
}

locals {
  app_name = "httpd"
}

locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}

source "null" "example1" {
  communicator = "none"
}

source "null" "example2" {
  communicator = "none"
}

build {
  sources = ["source.null.example1", "source.null.example2"]
  provisioner "shell-local" {
    inline = ["echo not overridden"]
    override = {
      example1 = {
        inline = ["echo yes overridden"]
      }
    }
  }
}

source "amazon-ebs" "httpd" {
  ami_name      = "PACKER-DEMO-${local.app_name}"
  instance_type = "t2.micro"
  region        = "us-west-2"
  source_ami    = "${var.ami_id}"
  ssh_username  = "ec2-user"
  tags = {
    Env  = "DEMO"
    Name = "PACKER-DEMO-${local.app_name}"
  }
}

source "docker" "example" {
  image = "ubuntu"
  commit = true
    changes = [
    "USER www-data",
    "WORKDIR /var/www",
    "ENV HOSTNAME www.example.com",
    "VOLUME /test1 /test2",
    "EXPOSE 80 443",
    "LABEL version=1.0",
    "ONBUILD RUN date",
    "CMD [\"nginx\", \"-g\", \"daemon off;\"]",
    "ENTRYPOINT /var/www/start.sh"
  ]
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "${var.ami_prefix}-${local.timestamp}"
  instance_type = "t2.micro"
  region        = "us-west-2"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  ssh_username = "ubuntu"
}

build {
  sources = ["source.amazon-ebs.httpd"]

  provisioner "shell" {
    script = "script/script.sh"
  }

  provisioner "file" {
    source = "/build/artifact/code.jar"
    destination = "/app/code.jar"
  }

  provisioner "ansible" {
    playbook_file           = "./ansible/playbook.yml"
    user                    = "Administrator"
    use_proxy               = false
    extra_arguments         = [
      "--extra-vars",
      "@vars.yml", # or
      "ansible_host         = ${var.ansible_host} ansible_connection=${var.ansible_connection}"
    ]
    ansible_env_vars        = ["PACKER_BUILD_NAME={{ build_name }}"]
    inventory_file_template = "{{ .HostAlias }} ansible_host={{ .ID }} ansible_user={{ .User }} ansible_ssh_common_args='-o StrictHostKeyChecking=no -o ProxyCommand=\"sh -c \\\"aws ssm start-session --target %h --document-name AWS-StartSSHSession --parameters portNumber=%p\\\"\"'\n"
  }

  post-processor "shell-local" {
    inline = ["echo foo"]
  }

  post-processor "vagrant" {}

  post-processor "compress" {
    compression_level = 9
    output = ami.tar
    keep_input_artifact = true
  }

  post-processor "docker-tag" {
    repository = "12345.dkr.ecr.us-east-1.amazonaws.com/packer"
    tags = ["0.7"]
  }

  post-processor "docker-push" {
    ecr_login = true
    aws_access_key = "YOUR KEY HERE"
    aws_secret_key = "YOUR SECRET KEY HERE"
    login_server = "https://12345.dkr.ecr.us-east-1.amazonaws.com/"
  }
}

build {
  sources = ["source.docker.example"]
}

